<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDefaultTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brands', function (Blueprint $table) {
            $table->increments('id');
            $table->string('brand_name');
            $table->timestamps();
        });

        Schema::create('carmodels', function(Blueprint $table){
            $table->increments('id');
            $table->integer('brand_id');
            $table->string('model_name');
            $table->string('engine');
            $table->integer('doors');
            $table->enum('type', ['GAS', 'DIESEL', 'ELECTRIC', 'HYBRID']);
            $table->timestamps();
        
        });

        Schema::create('cars', function(Blueprint $table){
            $table->increments('id');
            $table->integer('model_id');
            $table->integer('user_id');
            $table->integer('milage');
            $table->integer('price');
            $table->integer('year');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('default_tables');
    }
}
