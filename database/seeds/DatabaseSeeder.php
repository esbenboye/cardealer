<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // TODO: Create relations to make sure no broken data exists

        $brands = [
            ['id' => 1, 'brand_name' => 'Audi'],
            ['id' => 2, 'brand_name' => 'Mercedes'],
            ['id' => 3, 'brand_name' => 'Toyota'],
            ['id' => 4, 'brand_name' => 'Citroen'],
            ['id' => 5, 'brand_name' => 'Peugeot']
        ];

        $brandModel = new \App\Model\Brand();
        $brandModel->createMany($brands);


        $models = [
            ['brand_id' => 1, 'model_name' => 'A1', 'engine' => 'v1', 'doors' => 4, 'type' => 'GAS'],
            ['brand_id' => 1, 'model_name' => 'A2', 'engine' => 'v2', 'doors' => 4, 'type' => 'DIESEL'],
            ['brand_id' => 1, 'model_name' => 'A3', 'engine' => 'v3', 'doors' => 4, 'type' => 'ELECTRIC'],
            ['brand_id' => 1, 'model_name' => 'A4', 'engine' => 'v4', 'doors' => 4, 'type' => 'HYBRID'],
            
            ['brand_id' => 2, 'model_name' => 'M1', 'engine' => 'v1', 'doors' => 4, 'type' => 'GAS'],
            ['brand_id' => 2, 'model_name' => 'M2', 'engine' => 'v2', 'doors' => 4, 'type' => 'DIESEL'],
            ['brand_id' => 2, 'model_name' => 'M3', 'engine' => 'v3', 'doors' => 4, 'type' => 'ELECTRIC'],
            ['brand_id' => 2, 'model_name' => 'M4', 'engine' => 'v4', 'doors' => 4, 'type' => 'HYBRID'],

            ['brand_id' => 3, 'model_name' => 'T1', 'engine' => 'v1', 'doors' => 4, 'type' => 'GAS'],
            ['brand_id' => 3, 'model_name' => 'T2', 'engine' => 'v2', 'doors' => 4, 'type' => 'DIESEL'],
            ['brand_id' => 3, 'model_name' => 'T3', 'engine' => 'v3', 'doors' => 4, 'type' => 'ELECTRIC'],
            ['brand_id' => 3, 'model_name' => 'T4', 'engine' => 'v4', 'doors' => 4, 'type' => 'HYBRID'],

            ['brand_id' => 4, 'model_name' => 'C1', 'engine' => 'v1', 'doors' => 4, 'type' => 'GAS'],
            ['brand_id' => 4, 'model_name' => 'C2', 'engine' => 'v2', 'doors' => 4, 'type' => 'DIESEL'],
            ['brand_id' => 4, 'model_name' => 'C3', 'engine' => 'v3', 'doors' => 4, 'type' => 'ELECTRIC'],
            ['brand_id' => 4, 'model_name' => 'C4', 'engine' => 'v4', 'doors' => 4, 'type' => 'HYBRID'],

            ['brand_id' => 5, 'model_name' => 'P1', 'engine' => 'v1', 'doors' => 4, 'type' => 'GAS'],
            ['brand_id' => 5, 'model_name' => 'P2', 'engine' => 'v2', 'doors' => 4, 'type' => 'DIESEL'],
            ['brand_id' => 5, 'model_name' => 'P3', 'engine' => 'v3', 'doors' => 4, 'type' => 'ELECTRIC'],
            ['brand_id' => 5, 'model_name' => 'P4', 'engine' => 'v4', 'doors' => 4, 'type' => 'HYBRID'],
        ];

        $modelModel = new \App\Model\Carmodel();
        $modelModel->createMany($models);


        $users = [
            ['name' => 'Arne', 'email' => 'arne@test.com','password' => bcrypt('secret'), 'apikey' => md5('hello')],
            ['name' => 'Bente', 'email' => 'bente@test.com','password' => bcrypt('secret'), 'apikey' => md5('world')],
        ];

        foreach($users as $userData){
            $userModel = new \App\User($userData);
            $userModel->save();
        }


        $cars = [
            ['model_id' => 1, 'user_id' => 1, 'milage' => 100, 'price' => 500, 'year' => 1999],
            ['model_id' => 5, 'user_id' => 1, 'milage' => 200, 'price' => 1900, 'year' => 1994],
            ['model_id' => 9, 'user_id' => 1, 'milage' => 300, 'price' => 2500, 'year' => 1989],
            ['model_id' => 13, 'user_id' => 1, 'milage' => 400, 'price' => 3700, 'year' => 1978],
            ['model_id' => 17, 'user_id' => 1, 'milage' => 500, 'price' => 4900, 'year' => 1962],
            ['model_id' => 1, 'user_id' => 2, 'milage' => 600, 'price' => 6000, 'year' => 1999],
            ['model_id' => 1, 'user_id' => 2, 'milage' => 700, 'price' => 7100, 'year' => 1995],
            ['model_id' => 1, 'user_id' => 2, 'milage' => 800, 'price' => 8200, 'year' => 1992],
            ['model_id' => 1, 'user_id' => 2, 'milage' => 900, 'price' => 9300, 'year' => 1892],
            ['model_id' => 1, 'user_id' => 2, 'milage' => 1000, 'price' => 9900, 'year' => 1992],
        ];

        $carModel = new \App\Model\Car();
        $carModel->createMany($cars);

    }
}
