<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Should be placed in seperate controller
Route::get('/', function () {
    return view('welcome', [
        'js' => [
            'mockService.js',
            'defaultController.js'
        ]
    ]);
});

Auth::routes();

Route::get('/home','HomeController@index')->name('home.index');
Route::get('/home/{id}/edit', 'HomeController@edit')->name('home.edit');
Route::post('/home/{id}/edit', 'HomeController@update')->name('home.update');
Route::delete('/home/{id}/delete', 'HomeController@destroy')->name('home.delete');
Route::post('/home', 'HomeController@store')->name('home.store');
Route::group(['prefix' => 'api/v1'], function(){
    Route::get('cars/browse','CarsController@browse');
    Route::get('cars/{id}', 'CarsController@get');

    Route::group(['middleware' => 'apikey'], function(){
        Route::get('cars', 'ApiController@getAll');
        Route::delete('cars/{id}', 'ApiController@delete');
        Route::post('cars', 'ApiController@create');
        Route::post('cars/{id}', 'ApiController@update');
    });

});