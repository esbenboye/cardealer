@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @foreach ($errors->all() as $error)
                        <div class="alert alert-danger" role="alert">{{ $error }}</div>
                    @endforeach
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div>

                        <form action="{{ route('home.store' )}}" method="post">
                            <select name="brand_id">
                                <option value="1">Audi</option>
                            </select>

                            <select name="model_id">
                                <option value="1">A1</option>
                                <option value="2">A2</option>
                                <option value="3">A3</option>
                            </select>
                            @csrf
                             
                          @include('form.vertical.text', ['label' => 'Milage', 'id' => 'milage', 'value' => '','name' => 'milage'])
                          @include('form.vertical.text', ['label' => 'Year', 'id' => 'year', 'value' => '', 'name' => 'year'])
                          @include('form.vertical.text', ['label' => 'Price', 'id' => 'price', 'value' => '', 'name' => 'price'])
                            <input type="submit" value="gem" class="btn btn-primary">
                       </form>



                        <table class="table">
                            <tr>
                                <th>id</th>
                                <th>Brand</th>
                                <th>Model</th>
                                <th>Milage</th>
                                <th>Year</th>
                                <th>Price</th>
                                <th></th>
                                <th></th>
                            </tr>
                            @foreach($cars as $car)
                            <tr>
                                <td>{{$car->id}}</td>
                                <td>{{$car->carmodel->brand->brand_name}}</td>
                                <td>{{$car->carmodel->model_name}}</td>
                                <td>{{$car->milage}}</td>
                                <td>{{$car->year}}</td>
                                <td>{{$car->price}}</td>
                                <th><a href="{{Route('home.edit', $car->id) }}">Edit</a></th>
                                <th>
                                    <form action="{{ route('home.delete', ['id' => $car->id]) }}" method="post" id="deleteForm_{{ $car->id }}">
                                        @csrf
                                        <input type="hidden" name="_method" value="delete">
                                    <a href="#" onclick="document.getElementById('deleteForm_{{ $car->id }}').submit()">Delete</a>
                                    </form>
                                </th>
                            </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
