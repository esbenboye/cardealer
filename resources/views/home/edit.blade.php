@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Edit car</div>

                <div class="card-body">
                    @foreach ($errors->all() as $error)
                        <div class="alert alert-danger" role="alert">{{ $error }}</div>
                    @endforeach
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div>
                        <form action="/home/{{$car->id}}/edit" method="post">
                            <h1>{{ $car->carmodel->brand->brand_name }} {{ $car->carmodel->model_name }}</h1>
                            @csrf
                             
                          @include('form.vertical.text', ['label' => 'Milage', 'id' => 'milage', 'value' => $car->milage,'name' => 'milage'])
                          @include('form.vertical.text', ['label' => 'Year', 'id' => 'year', 'value' => $car->year, 'name' => 'year'])
                          @include('form.vertical.text', ['label' => 'Price', 'id' => 'price', 'value' => $car->price, 'name' => 'price'])
                            <input type="submit" value="gem" class="btn btn-primary">
                       </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
