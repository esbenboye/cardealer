<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>

        <script src="{{ asset('js/angular.min.js') }}"></script>
        <script src="{{ asset('js/angular.app.js') }}"></script>
        @if(isset($js) && is_array($js))
            @foreach($js as $file)
                <script src="{{ asset("js/$file") }}"></script>
            @endforeach
        @endif

    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content" ng-app="CarDealer" ng-controller="defaultController">
                @verbatim

                <!-- TODO: Do not use tables for this, please... -->
                <table>
                    <tr>
                        <td>
                            Brand:
                        </td>
                        <td>
                            <select ng-options="brand.brand_name for brand in brands" ng-model="currentBrand" ng-change="currentModel=currentBrand.models[0]"></select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Model:
                        </td>
                        <td>
                            <select ng-options="model.model_name for model in currentBrand.models" ng-model="currentModel"></select>
                        </td>
                    </tr>

                    <tr>
                        <td></td>
                    </tr>

                    <tr>
                        <td>Milage, min:</td>
                        <td><input type="text" ng-model="milage_min"></td>
                    </tr>
                    <tr>
                        <td>Milage, max:</td>
                        <td><input type="text" ng-model="milage_max"></td>
                    </tr>
                    <tr>
                        <td>Price, min:</td>
                        <td><input type="text" ng-model="price_min"></td>
                    </tr>
                    <tr>
                        <td>Price, max:</td>
                        <td><input type="text" ng-model="price_max"></td>
                    </tr>
                    <tr>
                        <td>Year, max:</td>
                        <td><input type="text" ng-model="year_max"></td>
                    </tr>
                    <tr>
                        <td>Year, min:</td>
                        <td><input type="text" ng-model="year_min"></td>
                    </tr>
                </table>
                    <input type="button" ng-click="loadCars()" value="Filter">
                    <input type="button" ng-click="reset()" value="nulstil">
                </table>


                <table>
                    <tr>
                        <th>Brand</th>
                        <th>Model</th>
                        <th>Engine</th>
                        <th>Doors</th>
                        <th>Type</th>
                        <th>Milage</th>
                        <th>Price</th>
                        <th>Year</th>
                        <th>show</th>
                    </tr>
                    <tr ng-repeat="car in cars">
                        <td>{{ car.carmodel.brand.brand_name }}</td>
                        <td>{{ car.carmodel.model_name }}</td>
                        <td>{{ car.carmodel.engine }}</td>
                        <td>{{ car.carmodel.doors }}</td>
                        <td>{{ car.carmodel.type }}</td>
                        <td>{{ car.milage }}</td>
                        <td>{{ car.price }}</td>
                        <td>{{ car.year }}</td>
                        <td><a ng-click="showCar(car)">Click to show</a></td>
                    </tr>
                </table>

                <div ng-if="currentCar">
                <b>Show fance car here:</b><br>
                Brand: {{ currentCar.carmodel.brand.brand_name }}<br>
                Model: {{ currentCar.carmodel.model_name }}<br>
                Engine: {{ currentCar.carmodel.engine }}<br>
                Doors: {{ currentCar.carmodel.doors }}<br>
                Type: {{ currentCar.carmodel.type }}</br>
                Milage: {{ currentCar.milage }}<br>
                Price:{{ currentCar.price }}<br>
                Year:{{ currentCar.year }}<br>
                </div>
                @endverbatim
            </div>
        </div>
    </body>
</html>
