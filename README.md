## Requirements

1. A local environemnt (php + mysql). Docker is recommended.
2. Git

## Nice to have
1. PostMan - Either standalone or browser extension (for detailed api example requests)

## Getting started
1. Boot up your environment - Check `.env` and `docker-compose.yml` file for credentials and ports.
2. Install using `composer install`
3. Run `php artisan migrate` to create tables
4. Run `php artisan db:seed` to fill database with sample data

## General about endpoints

When sending a request to the api, it will return in JSON format. On success, the response will be 200 OK and the response will look like this;

```
{
  car:{
    brand:'Audi',
    model:'A1',
    engine:'V3'
    year:2005
  },
  status: 200
}
```

On error, a message will be given along with a fitting message, like
```
{
  status: 404,
  message: 'Brand [4] was not found'
}
```

Endpoints that are expected to return only one car, will return the car property as an object. If a search or listing is performed, then the car property will be plural (i.e. cars) and an array with cars will be given. 

```
{
  cars:[
    {},
    {},
    {},
    {}
  ],
  status: 200
}
```


Please refer to the PostMan collection for further details.


## How to browse/search 

Use GET query params to search/filter the cars:

`GET /api/v1/cars/browse?[column]__[comparitor]=[value]&order=[column]:[direction]`

Params `id__eq=10` will return the car where id is equal to 10

Params `price__gt=1` will return the car where price is greater than 1

Params `created_at__gt=2018-01-01` will return the cars created later than 2018-01-01

Params `order=id:asc` will order by id, ascending

Params `order=created_at:asc` will order by id, ascending

If you add a `with` parameter, additional information will be available;


`?with=carmodel` will also yield the model of the car


`?with=carmodel.brand` will give both the model and the brand. 

### The following comparators are available:

`gt` - Greater than

`gte` - Greater than or equal to

`lt` - Less than

`lte` - Less than or equal to

`eq` - Equals

All columns are currently available for all comparisons and it is possible to use multiple conditions (e.g. `?brand__eq=Audi&id__gt=10`)

## Private API endpoints. 
Requires apikey, that must be sent as a query params, i.e. `api/v1/cars?apikey=[...]`

`POST /api/v1/cars` - Create car. Returns created car.

`POST /api/v1/cars/{id}` - Update car. Returns updated car.

`DELETE /api/v1/cars/{id}` - Delete a car. Returns 200 OK

See Postman Collection (delete and create) for further details.

## Public endpoints. 

Does not require an apikey

`GET /api/v1/cars` - Search/browse cars. Returns found cars.

`GET /api/v1/cars/{id}` - Get details on given car. Returns given car. 

## Rough plan
1. + Set up Laravel
2. + Build tables (migration)
3. + Seed data (10 brands with 5 models each, 2 users with 5 cars each)
4. + Set up Auth and ApiKey middleware
5. + Build api + Documentation
6. - Build automated integration tests
7. + Build guest area 
8. - Build dashboard

##Todo:
 - Error handling
 - Input validation
 - Better documentation on endpoints
 - Facade + Serviceprovider for services
 - Move middleware from controllers to routes
 - Use RequestClasses and not data array
 - Make more request classes
 - Dependency inject classes
 - Move texts from const to language
 - Expand relations to have both has and belongs
 - Use custom Docker-compose
 - Use built-in auth:api middleware
 - Use Trait instead of BaseModel
 - Skip api part
