app.service("mockService", [function(){
    return {
        getBrands: function(){

            return [
                {
                    brand_name: 'Audi', 
                    id: 1,
                    models:[
                        {'model_name': 'a1', id:1},
                        {'model_name': 'a2', id:2},
                        {'model_name': 'a3', id:3},
                    ]

                },
                {
                    brand_name: 'Mercedes', 
                    id: 2,
                    models:[
                        {'model_name': 'm1', id:5},
                        {'model_name': 'm2', id:6},
                        {'model_name': 'm3', id:7},
                    ]
                },
                {
                    brand_name: 'Toyota', 
                    id: 3,
                    models:[
                        {'model_name': 't1', id:9},
                        {'model_name': 't2', id:10},
                        {'model_name': 't3', id:11},
                    ]
                },
                {
                    brand_name: 'Citroen',
                     id: 4,
                     models:[
                        {'model_name': 'c1', id:13},
                        {'model_name': 'c2', id:14},
                        {'model_name': 'c3', id:15},
                    ]
                },
                {
                    brand_name: 'Peugeot',
                    id: 5,
                    models:[
                        {'model_name': 'p1', id:17},
                        {'model_name': 'p2', id:18},
                        {'model_name': 'p3', id:19},
                    ]
                }

            ]

        }



    }
}])