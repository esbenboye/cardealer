app.controller('defaultController', ['$http','$scope', 'mockService', function($http, $scope, mockService){
    
    // Variables first
    $scope.params = {
        with:'carmodel.brand'

    }

    $scope.brands = mockService.getBrands();
    $scope.currentCar;
    // Then functions
    $scope.loadCars = function(){

        var searchTerms = {
            with:'carmodel.brand'
        };

        if($scope.milage_min){
            searchTerms.milage__gt = $scope.milage_min;
        }

        if($scope.milage_max){
            searchTerms.milage__lt = $scope.milage_max;
        }

        if($scope.price_min){
            searchTerms.milage__gt = $scope.price_min;
        }

        if($scope.price_max){
            searchTerms.price_lt = $scope.price_max;
        }

        if($scope.year_max){
            searchTerms.price_lt = $scope.year_max;
        }

        if($scope.year_min){
            searchTerms.price_gt = $scope.year_min;
        }
        if($scope.currentModel){
            searchTerms.model_id__eq = $scope.currentModel.id;    
        }
        

        var params = {
            params: searchTerms
        };

        // This should be put in a service!
        $http.get('/api/v1/cars/browse', params)
            .success(function(data){
                $scope.cars = data.cars;
            })
            .error(function(data){
                console.log("ERROR");
            });
    }

    $scope.reset = function(){
        $scope.currentBrand = {};
        $scope.currentModel = {};
        $scope.milage_min = "";
        $scope.milage_max = "";
        $scope.price_min = "";
        $scope.price_max = "";
        $scope.year_min = "";
        $scope.year_max = "";
        $scope.loadCars();
    }

    $scope.showCar = function(car){
        $scope.currentCar = car;
    }


    // And finally init stuff
    $scope.loadCars();
}]);