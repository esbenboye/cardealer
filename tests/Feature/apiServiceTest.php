<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\User;
use App\Service\ApiService;
use Exception;

class apiServiceTest extends TestCase
{
    use DatabaseMigrations;

    const DEFAULT_ENDPOINT = '/api/v1';

    public function setUp(){
        parent::setUp();
        $this->artisan("db:seed");
        $this->user = User::find(1);
        $this->service = new ApiService();
        
    }
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }

    public function testThatUserCanCreateCar(){
        $carData = [
            'model_id' => 1,
            'milage' => 500,
            'price' => 200,
            'year' => 1984
        ];

        $newCar = $this->service->create($this->user, $carData);

        foreach($carData as $key => $value){
            $this->assertEquals($value, $newCar->$key);
        }
    }

    public function testThatUserCanOnlyUpdateOwnCars(){
        $this->expectException(Exception::class);        
        $this->service->update($this->user, 14, []);
    }

    public function testThatUserCanUpdateOwnCar(){

        $newData = [
            'milage' => 1500,
            'price' => 999
        ];

        $updatedCar = $this->service->update($this->user, 2, $newData);

        $this->assertEquals($updatedCar->milage, $newData['milage']);
        $this->assertEquals($updatedCar->price, $newData['price']);
    }

    public function testThatUserCanDeleteOwnCar(){
        $response = $this->service->delete($this->user, 2);
        $this->assertTrue($response);
    }

    public function testThatUserCannotDeleteOtherCars(){
        $this->expectException(Exception::class);
        $this->service->delete($this->user, 14);
    }

}
