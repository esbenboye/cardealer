<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\User;
use App\Service\ApiService;
use Exception;

class baseTest extends TestCase
{
    use DatabaseMigrations;

    const DEFAULT_ENDPOINT = '/api/v1';

    public function setUp(){
        parent::setUp();
        $this->artisan("db:seed");
        $this->user = User::find(1);
        
    }

    // Helper functions
    public function sendGetRequest($url, $query, $asArray = false){
        $url = self::DEFAULT_ENDPOINT.$url."?".http_build_query(($query));
        $response = $this->get($url);
        return json_decode($response->getContent(), $asArray);
    }

    public function sendPostRequest($url, $query, $params){
        $url = self::DEFAULT_ENDPOINT.$url."?".http_build_query($query);
        $response = $this->post($url, $params);
        return json_decode($response->getContent());
    }

    public function sendDeleteRequest($url, $query = [], $params = []){
        $url = self::DEFAULT_ENDPOINT.$url."?".http_build_query($query);
        $response = $this->delete($url, $params);
        return json_decode($response->getContent());
    }

    public function testTest(){
        $this->assertTrue(true);
    }

}
