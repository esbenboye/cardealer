<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class apiFilterTest extends baseTest
{
    public function testEqFilter(){
        $params = [
            'user_id__eq' => 1
        ];

        $response = $this->sendGetRequest('/cars/browse', $params);
        $this->assertEquals($response->status, 200);
        foreach($response->cars as $car){
            $this->assertEquals($car->user_id, 1);
        }
    }

    public function testGtFilter(){
        $params = [
            'id__gt' => 10
        ]; 
        $response = $this->sendGetRequest('/cars/browse', $params);
        $this->assertEquals($response->status, 200);

        foreach($response->cars as $car){
            $this->assertTrue($car->id > 10);
        }
    }

    public function testGteFilter(){
        $params = [
            'id__gte' => 10
        ]; 
        $response = $this->sendGetRequest('/cars/browse', $params);
        $this->assertEquals($response->status, 200);

        foreach($response->cars as $car){
            $this->assertTrue($car->id >= 10);
        }
    }

    public function testLtFilter(){
        $params = [
            'id__lt' => 10
        ]; 
        $response = $this->sendGetRequest('/cars/browse', $params);
        $this->assertEquals($response->status, 200);

        foreach($response->cars as $car){
            $this->assertTrue($car->id < 10);
        }
    }

    public function testInFilter(){
        $params = [
            'id__lte' => 10
        ]; 
        $response = $this->sendGetRequest('/cars/browse', $params);
        $this->assertEquals($response->status, 200);

        foreach($response->cars as $car){
            $this->assertTrue($car->id <= 10);
        }
    }

    public function testNinFilter(){
        $exclude = [
            2, 4, 6, 8, 10, 12, 14, 16, 18, 20
        ];
        $params = [
            'id__nin' => implode(",",$exclude)
        ]; 
        $response = $this->sendGetRequest('/cars/browse', $params);
        $this->assertEquals($response->status, 200);

        foreach($response->cars as $car){
            $this->assertFalse(in_array($car->id, $exclude));
        }
    }

    public function testWithParam(){
        $params = ['with' => 'carmodel.brand'];
        $response = $this->sendGetRequest('/cars/browse', $params, true);
        $this->assertEquals($response['status'], 200);
        foreach($response['cars'] as $car){
            $this->assertTrue(isset($car['carmodel']));
            $this->assertTrue(isset($car['carmodel']['brand']));
        }

    }

    public function testMultipleFilters(){
        $params = [
            'id__gt' => 5,
            'user_id__eq' => 2,
            'price__gte' => 200, 
            'year__lt' => 1999
        ];
        $response = $this->sendGetRequest('/cars/browse', $params);

        foreach($response->cars as $car){
            $this->assertTrue($car->id > 5);
            $this->assertTrue($car->user_id == 2);
            $this->assertTrue($car->price >= 200);
            $this->assertTrue($car->year < 1999);
        }

        $this->assertEquals(count($response->cars), 4);
    }

}
