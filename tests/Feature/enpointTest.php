<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\User;
use App\Service\ApiService;
use Exception;

class endpointTest extends baseTest
{
    use DatabaseMigrations;

    public function testThatUserCanCreateCar(){
        $carData = [
            'model_id' => 1,
            'milage' => 100, 
            'price' => 200,
            'year' => 1984
        ]; 

        $createdCar = $this->sendPostRequest('/cars', ['apikey' => $this->user->apikey], $carData);
        foreach($carData as $key => $value){
            $this->assertEquals($createdCar->car->$key, $value);
        }
    }

    public function testThatUserCanOnlyUpdateOwnCars(){
        $this->expectException(Exception::class);        
        $this->service->update($this->user, 14, []);
    }

    public function testThatUserCanUpdateOwnCar(){
        $response = $this->sendGetRequest("/cars", ['apikey' => $this->user->apikey]);
        foreach($response->cars as $car){
            $this->assertEquals($car->user_id, $this->user->id);
        }
        $this->assertEquals($response->status, 200);
    }

    public function testThatUserCanDeleteOwnCar(){
        $response = $this->sendDeleteRequest('/cars/1', ['apikey' => $this->user->apikey]);
        $this->assertEquals($response->status, 200);
    }

    public function testThatUserCannotDeleteOtherCars(){
        $response = $this->sendDeleteRequest('/cars/14', ['apikey' => $this->user->apikey]);
        $this->assertEquals($response->status, 404);
        $this->assertEquals($response->message, 'Car not found');
    }

    public function testThatApikeyIsRequired(){
        $response = $this->sendPostRequest('/cars', [], []);
        $this->assertEquals($response->status, 400);
        $this->assertEquals($response->message, 'Apikey must be supplied');
    }

}
