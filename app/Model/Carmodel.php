<?php

namespace App\Model;


/**
 * App\Model\CarModel
 *
 * @property-read \App\Model\Brand $brand
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\CarModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\CarModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\CarModel query()
 * @mixin \Eloquent
 * @property int $id
 * @property int $brand_id
 * @property string $model_name
 * @property string $engine
 * @property int $doors
 * @property string $type
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\CarModel whereBrandId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\CarModel whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\CarModel whereDoors($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\CarModel whereEngine($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\CarModel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\CarModel whereModelName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\CarModel whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\CarModel whereUpdatedAt($value)
 */
class CarModel extends BaseModel
{
    protected $table = "carmodels";

    public function brand(){
        return $this->hasOne(Brand::class, 'id','brand_id');
    }
}
