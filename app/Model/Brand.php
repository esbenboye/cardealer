<?php

namespace App\Model;



/**
 * App\Model\Brand
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Brand newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Brand newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Brand query()
 * @mixin \Eloquent
 * @property int $id
 * @property string $brand_name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Brand whereBrandName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Brand whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Brand whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Brand whereUpdatedAt($value)
 */
class Brand extends BaseModel
{
    

}
