<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Model\BaseModel
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\BaseModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\BaseModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\BaseModel query()
 * @mixin \Eloquent
 */
class BaseModel extends Model
{
    public function CreateMany(array $objects){

        foreach($objects as $info){
            $model = new $this($info);
            $model->save();
        }

    }
}
