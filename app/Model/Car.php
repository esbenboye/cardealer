<?php

namespace App\Model;

/**
 * App\Model\Car
 *
 * @property-read \App\Model\CarModel $carmodel
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Car newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Car newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Car query()
 * @mixin \Eloquent
 * @property int $id
 * @property int $model_id
 * @property int $user_id
 * @property int $milage
 * @property int $price
 * @property int $year
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Car whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Car whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Car whereMilage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Car whereModelId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Car wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Car whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Car whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Car whereYear($value)
 */
class Car extends BaseModel
{
    protected $fillable = ['model_id', 'user_id','milage','price','year'];

    public function carmodel(){
        return $this->hasOne(Carmodel::class, 'id','model_id');
    }



}
