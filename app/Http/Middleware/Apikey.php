<?php

namespace App\Http\Middleware;

use Closure;
use App\User;
use Auth;
class Apikey
{
    const NO_APIKEY = 'Apikey must be supplied';
    const INVALID_APIKEY = 'Apikey [%s] is invalid';
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!$request->get('apikey')){
            $response = [
                'status' => 400,
                'message' => self::NO_APIKEY
            ];
            return response()->json($response);
        }

        $apikey = $request->get('apikey');
        $user = User::where('apikey', $apikey)->first();

        if(!$user){
            $response = [
                'status' => 400,
                'message' => sprintf(self::INVALID_APIKEY, $apikey)
            ];
            return response()->json($response);
        }

        Auth::loginUsingId($user->id, false);

        return $next($request);
    }
}
