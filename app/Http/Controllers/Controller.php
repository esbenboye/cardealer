<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected function handleSuccess($index, $obj){
        return [
            $index => $obj,
            'status' => 200
        ];
    }

    protected function handleError($e){
        return [
            'status' => $e->getCode(),
            'message' => $e->getMessage()
        ];
    }
}
