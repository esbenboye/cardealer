<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreCarRequest;
use App\Http\Requests\UpdateCarRequest;
use Illuminate\Http\Request;
use Auth;
use Exception;

class ApiController extends Controller
{
    public function __construct(){
        $this->apiService = new \App\Service\ApiService();
    }

    public function create(StoreCarRequest $request){
        $user = Auth::user();
        $data = [
            'model_id' => $request->get('model_id'),
            'milage' => $request->get('milage'),
            'price' => $request->get('price'),
            'year' => $request->get('year')
        ];
        $car = $this->apiService->create($user, $data);
        return $this->handleSuccess('car', $car);
    }

    public function getAll(){
        $user = Auth::user();
        $cars = $this->apiService->getAll($user);
        return $this->handleSuccess('cars', $cars);
    }

    public function get($id){
        $user = Auth::user();
        $cars = $this->apiService->get($user, $id);
        return $this->handleSuccess('cars',$cars);
    }

    public function update(UpdateCarRequest $request, $id){
        $user = Auth::user();
        $data = $request->all();
        $car = $this->apiService->update($user, $id, $data);
        return $this->handleSuccess('car', $car);

    }

    public function delete(Request $request, $id){
        $user = Auth::user();
        try{
            $status = $this->apiService->delete($user, $id);    
            return ['status' => 200];
        }catch(Exception $e){
            return $this->handleError($e);
        }
    }
}
