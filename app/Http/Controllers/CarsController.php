<?php

namespace App\Http\Controllers;

use CarService;
use Illuminate\Http\Request;
use Exception;
use App\Model\Car;

class CarsController extends Controller
{
  public function get ($id)
  {
    try {
      $result = CarService::get($id);
      return [
        'car' => $result,
        'status' => 200
      ];
    } catch (Exception $e) {
      return $this->handleError($e);
    }
  }

  public function browse (Request $request)
  {
    $params = $request->all();
    $model = new Car();

    try {
      $result = CarService::search($model, $params);
      return [
        'cars' => $result,
        'status' => 200
      ];
    } catch (Exception $e) {
      return $this->handleError($e);
    }
  }


}
