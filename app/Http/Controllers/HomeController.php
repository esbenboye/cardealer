<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreCarRequest;
use App\Http\Requests\UpdateCarRequest;
use Illuminate\Http\Request;
use App\Service\HomeService;
use App\Service\ApiService;
use Auth;

class HomeController extends Controller
{
    private $homeService;
    public function __construct()
    {
        $this->middleware('auth');
        $this->homeService = new HomeService();
        $this->apiService = new ApiService();
        
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $cars = $this->apiService->getAll($user);
        return view('home.index', ['cars' => $cars]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('home/cars/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCarRequest $request)
    {
        $data = [
            'model_id'  => $request->input('model_id'),
            'milage'    => $request->input('milage'),
            'price'     => $request->input('price'),
            'year'      => $request->input('year')
        ];
        $car = $this->apiService->create(Auth::user(), $data);
        if($car['status'] == 200){
            return redirect(route('home.index'));
        }else{
            return redirect(route('home.index'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $car = $this->apiService->get($id);
        return view('/home/cars/show', $car);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = Auth::user();
        $car = $this->apiService->get($user, $id);
        return view('home.edit', ['car' => $car]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCarRequest $request, $id)
    {
        $data = [
           'milage' => $request->input('milage'),
           'price' => $request->input('price'),
           'year' => $request->input('year')
        ]; 
        $car = $this->apiService->update(Auth::user(), $id, $data);
        if($car['status'] == 200){
            return redirect(route('home.index'));
        }else{
            return redirect(route('home.index'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = $this->apiService->delete(Auth::user(), $id);
        if($result['status'] == 200){
            return redirect(route('home.index'));
        }else{
            return redirect(route('home.index'));
        }
    }
}
