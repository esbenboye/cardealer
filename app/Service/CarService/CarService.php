<?php

namespace App\Service\CarService;

use App\Model\Car;
use \Exception;
use Auth;
use Log;

class CarService
{
  private $model;
  private $comparators = [
    'eq' => '=',
    'gt' => '>',
    'gte' => '>=',
    'lt' => '<',
    'lte' => '<=',
    'in' => 'in',
    'nin' => 'nin'
  ];


  const CAR_NOT_FOUND_BY_ID = "Car with id [%s] was not found";
  const UNAUTHORIZED_TO_DELETE = "Unauthorised to delete car";


  public function get ($id)
  {
    $car = Car::with('carmodel.brand')->find($id);


    if (is_null($car)) {
      throw new Exception(sprintf(self::CAR_NOT_FOUND_BY_ID, $id), 404);
    }

    return $car;
  }

  public function getAll ()
  {
    return Car::get();
  }

  public function update ($id, array $data)
  {
    $car = self::get($id);

    foreach ($data as $key => $value) {
      $car->$key = $value;
    }
    $car->save();
    return $car;

  }

  public function delete ($id)
  {
    $car = self::get($id);

    $user = Auth::user();
    if ($user->id == $car->user_id) {
      $car->delete();
      return true;
    } else {
      throw new Exception(sprintf(self::UNAUTHORIZED_TO_DELETE), $id);
    }
  }

  public function applyDefaultQuery ($model)
  {
    // Perform default query here ... E.g. with('') or select()
    //$model = $model->with('brand');
    return $model;
  }

  public function create ($data)
  {
    $user = Auth::user();
    $data['user_id'] = $user->id;

    $model = new Car($data);
    // $model->validate(); // TODO: Add validation that the car is correct. E.g. datatypes and that the model exists
    $model->save();
    return $model;
  }


  // Should be moved to seperate class that can be extended


  public function search ($model, $query)
  {
    $this->model = $this->applyDefaultQuery($model);
    $errors = [];
    // TODO: Add validation before select columns that does not exist
    foreach ($query as $search => $value) {
      if (strpos($search, '__') === false) {
        continue;
      }
      $raw = explode("__", $search);
      $field = $raw[0];
      $txt_comparator = $raw[1];

      if (array_key_exists($txt_comparator, $this->comparators)) {
        $comparator = $this->comparators[$txt_comparator];
        $this->addFilter($field, $comparator, $value);
      } else {
        $errors[] = sprintf(self::COMPARATOR_NOT_FOUND, $txt_comparator);
      }
    }
    if (isset($query['with'])) {
      $this->model = $this->model->with($query['with']);
    }

    if (count($errors) > 0) {
      throw new \Exception(implode("\n", $errors), 400);
    }
    return $this->model->get();
  }

  public function addFilter ($field, $comparator, $value)
  {
    switch (strtolower($comparator)) {
      case 'in':
        $this->model = $this->model->whereIn($field, explode(",", $value));
        break;
      case 'nin':
        $this->model = $this->model->whereNotIn($field, explode(",", $value));
        break;

      default:
        $this->model = $this->model->where($field, $comparator, $value);
        break;
    }
  }


}