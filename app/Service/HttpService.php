<?php
namespace App\Service;

use Auth;
use GuzzleHttp\Client;
use App\User;

class HttpService extends BaseService{
    
    protected $user;
    protected $connection;

    public function __construct(){
        $this->connection = new Client();
        $this->baseUrl = "http://127.0.0.1:7913/api/v1";
    }

    public function create($data){
        $payload = json_encode($data);
        $response = $this->connection->post($this->endpoint, $payload)->getContent();
        return json_decode($response);
    }

    public function getAll(User $user){
        $url = $this->baseUrl."/cars";
        $query = [
            'apikey' => $user->apikey
        ];
        $response = $this->connection->get(
            $url, 
            ['query'=> $query]
        )->getContent();
        return json_decode($response);
    }

    public function get($id){
        $url = $this->endpoint."/$id";
        $response = $this->connection->get($url)->getContent();
        return json_decode($response);
    }

    public function update($id, $data){
        $url = $this->endpoint."/".$id;
        $payload = json_encode($data);
        $response = $this->connection->post($url, $payload)->getContent();
        return json_decode($response);
    }

    public function delete($id){
        $url = $this->endpoint."/".$id;
        $response = $this->connection->delete($url)->getContent();
        return json_decode($response);
    }
}