<?php

namespace App\Service;
use App\User;
use App\Model\Car;
use Exception;

class ApiService{
    
    public function create(User $user, $data){
        $car = new Car($data);
        $car->user_id = $user->id;
        $car->save();
        return $car;
    }

    public function getAll(User $user, $with = ""){
        return Car::where('user_id', $user->id)->with('carmodel.brand')->get(); 
    }

    public function get(User $user, $id){
        return Car::where('id', $id)->where('user_id', $user->id)->first();
    }

    public function update(User $user, $id, $data){
        $car = Car::where('id',$id)->where('user_id', $user->id)->first();
        if(!$car){
            throw new Exception("Car not found", 404);
        }

        if(isset($data['model_id'])){
            $car->model_id = $data['model_id'];
        }

        if(isset($data['milage'])){
            $car->milage = $data['milage'];
        }

        if(isset($data['price'])){
            $car->price = $data['price'];
        }

        if(isset($data['year'])){
            $car->year = $data['year'];
        }
        
        $car->save();

        return $this->get($user, $id);
    }

    public function delete(User $user, $id){
        $car = Car::where('id', $id)->where('user_id', $user->id)->first();
        if(!$car){
            throw new Exception("Car not found",404);
        }

        $car->delete();
        return true;
    }



}