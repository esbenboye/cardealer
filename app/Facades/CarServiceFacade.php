<?php
namespace App\Service\CarService;

use Illuminate\Support\Facades\Facade;

class CarServiceFacade extends Facade
{
	protected static function getFacadeAccessor ()
	{
		return CarService::class;
	}
}